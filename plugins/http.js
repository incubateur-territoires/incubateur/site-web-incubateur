export default ({ $axios, $config }) => {
  $axios.setBaseURL($config.apiURL);
  $axios.setHeader('Authorization', `Bearer ${$config.apiToken}`);
};
