export const LOCALE = 'fr-FR';
export const DEFAULT_DATE_FORMAT = {
  day: 'numeric',
  month: 'long',
  year: 'numeric',
};
